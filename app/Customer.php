<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = ['ragione_sociale', 'nome', 'cognome', 'email', 'SSID', 'PEC', 'p_iva'];
}
