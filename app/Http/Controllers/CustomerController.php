<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customer/create');
    }


    public function store(Request $rest)
    {
        $input = $rest->all();
        Customer::create($input);
        return redirect('admin/home');
    }

    public function show(){

            $customers = Customer::all();

            return view('customer.show', compact('customers'));
        }


    //
}
