<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = Customer::all();

        return view('projects/create', compact('customers'));
    }


    public function store(Request $rest)
    {
        $input = $rest->all();
        Project::create($input);
        return redirect('admin/home');
    }

    public function show(){

        $projects  = Project::all();

        return view('customer.show', compact('$projects'));
    }


    //
    //
}
