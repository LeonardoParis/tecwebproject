<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = ['nome', 'descrizione', 'note', 'data_inizio', 'data_fine', 'cliente', 'costo_orario'];
}
