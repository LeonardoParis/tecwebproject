<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome');
            $table->string('descrizione');
            $table->string('note');
            $table->date('data_inizio');
            $table->date('data_fine');
            $table->bigInteger('cliente')->unsigned();
            $table->foreign('cliente')->references('id')->on('customers')->onDelete('cascade');
            $table->double('costo_orario');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
