@extends('layouts.app')

@section('content')

    <style>

        bigDiv{

            font-size: 30px;
        }
    </style>


    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard Admin</div>

                    <bigDiv class="card-body container" style="">
                        <div class="row">
                            <div class="" style="">
                                <a href="/customer/create">Aggiungi cliente</a>
                            </div>
                        </div>
                        <div class="row ">
                        <a href="/customer/show">Mostra clienti</a>
                        </div>
                        <div class="row ">
                            <a href="/projects/create">Crea progetto</a>
                        </div>
                    </bigDiv>
                </div>
            </div>
        </div>
    </div>
@endsection
