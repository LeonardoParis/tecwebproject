
@extends('layouts.app')

@section('content')



    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard Admin</div>

                    <div class="card-body">

                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">

                        <form  method="POST" action=" {{ URL::action('CustomerController@store') }}">
                        {{csrf_field()}}
                            <div class="form-group">
                                <label for="ragione_sociale">Ragione Sociale:</label>
                                <input type="text"  id="ragione_sociale" name="ragione_sociale">
                            </div>

                            <div class="form-group">
                                <label for="nome">Nome:</label>
                                <input type="text"  id="nome" name="nome">
                            </div>

                            <div class="form-group">
                                <label for="cognome">Cognome:</label>
                                <input type="text"  id="cognome" name="cognome">
                            </div>

                            <div class="form-group">
                                <label for="email">Email:</label>
                                <input type="text"  id="email" name="email">
                            </div>

                            <div class="form-group">
                                <label for="SSID">SSID:</label>
                                <input type="text"  id="SSID" name="SSID">
                            </div>

                            <div class="form-group">
                                <label for="PEC">PEC:</label>
                                <input type="text"  id="PEC" name="PEC">
                            </div>

                            <div class="form-group">
                                <label for="p_iva">P. IVA:</label>
                                <input type="text"  id="p_iva" name="p_iva">
                            </div>







                            <input type="submit" class=" btn btn-primary">

                        </form>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            </div>
        </div>

@endsection
