
@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard Admin</div>

                    <div class="card-body">

                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">

                                    <form  method="POST" action=" {{ URL::action('ProjectController@store') }}">

                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <label for="nome">Nome:</label>
                                            <input type="text"  id="nome" name="nome">
                                        </div>

                                        <div class="form-group">
                                            <label for="descrizione">Descrizione:</label>
                                            <input type="text"  id="descrizione" name="descrizione">
                                        </div>

                                        <div class="form-group">
                                            <label for="note">Note:</label>
                                            <input type="text"  id="note" name="note">
                                        </div>

                                        <div class="form-group">
                                            <label for="data_inizio">Data di inizio:</label>
                                            <input type="date"  id="data_inizio" required pattern="\d{4}-\d{2}-\d{2}" name="data_inizio">
                                        </div>

                                        <div class="form-group">
                                            <label for="data_fine">Data fine:</label>
                                            <input type="date"  id="data_fine"  required pattern="\d{4}-\d{2}-\d{2}" name="data_fine">
                                        </div>

                                        <div class="form-group">
                                            <label for="cliente">Cliente:</label>
                                            <select class="form-control" name="category_id">
                                                @foreach ($customers as $customer)
                                                    <option value="{{ $customer->id }}"> {{ $customer->nome }} </option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="">Costo orario:</label>
                                            <input type="number"  id="data_fine" name="data_fine">€
                                        </div>




                                        <input type="submit" class=" btn btn-primary">

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection
