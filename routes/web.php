<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

/*
    Questo metodo include le rotte per il login, registrazione, logout, e reset password.
    I controller di autenticazione si trovano nella cartella app/http/controller/auth.

    In Laravel, posso proteggere un percorso utilizzando un middleware:
    Lavel ha un middleware auth incorporato, che esiste in Illuminate\Auth\Middleware\Authenticate.

    È anche registrato nel kernel HTTP dell'app, posso aggiungerci le rotte che desidero per prevenire
    accessi agli utenti non autenticati.
*/
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('admin/home', 'HomeController@adminHome')->name('admin.home')->middleware('is_admin');
Route::get('/customer/create', 'CustomerController@create')->name('admin.create')->middleware('is_admin');
Route::post('/customer/create', 'CustomerController@store')->name('admin.store')->middleware('is_admin');
Route::get('/customer/show', 'CustomerController@show')->name('admin.create')->middleware('is_admin');
Route::get('/projects/create', 'ProjectController@create')->name('admin.create')->middleware('is_admin');
Route::post('/projects/create', 'ProjectController@store')->name('admin.store')->middleware('is_admin');
